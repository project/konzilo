<?php

/**
 * @file
 * Konzilo.
 */

/**
 * @return array
 */
function konzilo_job_resource_services() {
  return array(
    'job' => array(
      'index' => array(
        'help' => 'List all konzilo jobs',
        'callback' => '_konzilo_job_resource_index',
        'args' => array(
          array(
            'name' => 'resource_id',
            'optional' => TRUE,
            'type' => 'string',
            'description' => 'A resource id to filter on.',
            'default value' => 0,
            'source' => array('param' => 'resourceId'),
          ),
          array(
            'name' => 'done',
            'optional' => TRUE,
            'type' => 'boolean',
            'description' => 'A resource id to filter on.',
            'default value' => NULL,
            'source' => array('param' => 'done'),
          ),
        ),
        'access arguments' => array('view konzilo jobs'),
      ),
      'create' => array(
        'help' => 'Create a new job.',
        'callback' => '_konzilo_job_resource_create',
        'args' => array(
          array(
            'name' => 'job',
            'optional' => FALSE,
            'source' => 'data',
            'description' => 'The publish job to create',
            'type' => 'array',
          ),
        ),
        'access arguments' => array('create konzilo jobs'),
      ),
      'update' => array(
        'help' => 'Update a publishing job',
        'file' => array('type' => 'inc', 'module' => 'services', 'name' => 'resources/node_resource'),
        'callback' => '_konzilo_job_resource_update',
        'args' => array(
          array(
            'name' => 'jid',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The id of the job to update.',
          ),
          array(
            'name' => 'job',
            'optional' => FALSE,
            'source' => 'data',
            'description' => 'The job data',
            'type' => 'array',
          ),
        ),
        'access arguments' => array('update'),
        'access arguments append' => TRUE,
      ),
      'retrieve' => array(
        'callback' => '_konzilo_job_resource_retrieve',
        'access arguments' => array('view konzilo jobs'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'jid',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The id of this publish operation',
          ),
        ),
      ),
    ),
    'pubsettings' => array(
      'create' => array(
        'help' => 'Get publish settings for an article.',
        'callback' => '_konzilo_pubsettings_resource_create',
        'args' => array(
          array(
            'name' => 'article',
            'optional' => FALSE,
            'source' => 'data',
            'description' => 'The article to get settings for.',
            'type' => 'array',
          ),
        ),
        'access arguments' => array('access konzilo publish settings.'),
      ),
    ),
  );
}

/**
 * @param null $resource_id
 * @param null $done
 * @return array
 */
function _konzilo_job_resource_index($resource_id = NULL, $done = NULL) {
  $query = db_select('konzilo_job', 'j')->fields('j')->orderBy('created', 'desc');
  if (!empty($resource_id)) {
    $query->condition('resource_id', $resource_id);
  }
  if (isset($done)) {
    $query->condition('done', $done);
  }
  $result = array();
  foreach ($query->execute() as $job) {
    $job->done = !empty($job->done);
    $job->created = intval($job->created);
    $job->jid = intval($job->jid);
    if (isset($job->performed)) {
      $job->performed = intval($job->created);
    }
    $result[] = $job;
  }
  return $result;
}

/**
 * @param $job
 * @return mixed|object
 */
function _konzilo_job_resource_create($job) {
  if (empty($job['created'])) {
    $job['created'] = time();
  }
  watchdog('konzilo', print_r($job, TRUE));
  watchdog('konzilo', print_r($job['clientId'], TRUE));
  $endpoint = entity_load_single('konzilo_endpoint', $job['endpoint']);
  if (empty($endpoint)) {
    return services_error(check_plain('Could not find endpoint' . $job['endpoint']), 403);
  }
  $job['resource_type'] = $job['resourceType'];
  $job['resource_id'] = $job['resourceId'];
  $job = (object)$job;
  entity_save('konzilo_job', $job);
  if (module_exists('advancedqueue')) {
    $queue = DrupalQueue::get('konzilo_job_queue', TRUE);
    $task = array(
      'timestamp' => time(),
      'job_id' => $job->jid,
    );
    $queue->createItem($task);
  }
  return $job;
}

/**
 * @param $jid
 * @param $job
 * @return mixed
 */
function _konzilo_job_resource_update($jid, $job) {
  $old_job = entity_load_single('konzilo_job', $jid);
  if (empty($old_job)) {
    return services_error(t('Job not found'), 404);
  }
  entity_metadata_wrapper('konzilo_job', $job)->save();
  return $job;
}

/**
 * @param $jid
 * @return mixed
 */
function _konzilo_job_resource_retrieve($jid) {
  return entity_load_single('konzilo_job', $jid);
}

/**
 * @param $article
 * @return array
 */
function _konzilo_pubsettings_resource_create($article) {
  return module_invoke_all('konzilo_publish_settings', $article);
}