<?php

/**
 * @file
 * Konzilo.
 */

/**
 * Class KonziloEndpointEntity
 */
class KonziloEndpointEntity extends Entity {

  /**
   * @param $name
   * @return KonziloResource
   */
  public function resource($name) {
    return new KonziloResource($this, $name);
  }

  /**
   * @param $id
   * @return bool|mixed
   */
  public function getFile($id) {
    return $this->request("/file/$id/download/", array('json' => FALSE));
  }

  /**
   * @param $path
   * @param array $options
   * @return bool|mixed
   */
  public function request($path, $options = array()) {
    // Fetch a new access token if necessary.
    // @todo check when the token expires and fetch a new one.
    if (empty($this->access_token)) {
      $this->access_token = $this->getAccessToken();
      $this->save();
    }
    $url = $this->uri . $path;
    $defaults = array(
      'json' => TRUE,
      'headers' => array(
        'Authorization' => 'Bearer ' . $this->access_token,
      ),
    );
    $options += $defaults;
    $response = drupal_http_request($url, $options);
    if ($response->code == 200) {
      if (!empty($options['json'])) {
        return json_decode($response->data);
      }
      return $response->data;
    }
    return FALSE;
  }

  /**
   * @return bool
   */
  public function getAccessToken() {
    $data = array(
      'grant_type' => 'authorization_code',
      'code' => $this->refresh_token,
      'redirect_uri' => $this->uri,
    );
    $options = array(
      'method' => 'POST',
      'data' => http_build_query($data),
      'headers' => array(
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Authorization' => 'Basic ' . base64_encode($this->client_id . ':' . $this->client_secret),
      ),
    );
    $response = drupal_http_request($this->uri . '/oauth2/token', $options);
    if ($response->code == 200) {
      $data = json_decode($response->data);
      return $data->access_token;
    }
    return FALSE;
  }
}

/**
 * Class KonziloResource
 */
class KonziloResource {
  /**
   * @param $endpoint
   * @param $name
   */
  function __construct($endpoint, $name) {
    $this->endpoint = $endpoint;
    $this->name = $name;
  }

  /**
   * @param $id
   * @return mixed
   */
  function get($id) {
    $path = url($this->name . '/' . $id);
    return $this->endpoint->request($path);
  }

  /**
   * @param $q
   * @return mixed
   */
  function query($q) {
    $query = array(
      'q' => json_encode($q),
    );
    $path = url($this->name, array('query' => $query));
    return $this->endpoint->request($path);
  }
}
