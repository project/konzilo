<?php

/**
 * @file
 * Konzilo.
 */


/**
 * Class KonziloFileListPropertyMapper
 */
class KonziloFileListPropertyMapper extends PropertyMapper {
  /**
   * @param $fromProperty
   * @param $toProperty
   * @param string $targetDir
   */
  function __construct($fromProperty, $toProperty, $targetDir = 'public://') {
    parent::__construct($fromProperty, $toProperty);
    $this->targetDir = $targetDir;
  }

  /**
   * @param EntityMetadataWrapper $fromEntity
   * @param EntityMetadataWrapper $toEntity
   * @return EntityDrupalWrapper|void
   */
  function map(EntityMetadataWrapper $fromEntity, EntityMetadataWrapper $toEntity) {
    $newList = array();
    $property = $this->getFromValue($fromEntity, $this->fromProperty, TRUE);
    $endpoint = $fromEntity->get("endpoint")->value();
    if (empty($property) || empty($endpoint)) {
      return;
    }
    $info = $property->info();
    if (strstr($info['type'], 'list') !== FALSE) {
      $iterator = $property->getIterator();
    }
    else {
      $iterator = array($property);
    }
    foreach ($iterator as $wrapper) {
      $value = $wrapper->value();
      if (!empty($value)) {
        $id = $wrapper->get('_id')->value();
        $name = $wrapper->get('name')->value();
        if (!empty($id)) {
          $contents = $endpoint->getFile($id);
          if (!empty($contents)) {
            $file = file_save_data($contents, $this->targetDir . '/' . $name, FILE_EXISTS_REPLACE);
            if (!empty($file)) {
              // Support file fields.
              $file->display = 1;
              $newList[] = $file;
            }
          }
        }
      }
    }
    $toEntity->get($this->toProperty)->set($newList);
  }
}

/**
 * Class KonziloFilePropertyMapper
 */
class KonziloFilePropertyMapper extends PropertyMapper {
  /**
   * @param $fromProperty
   * @param $toProperty
   * @param string $targetDir
   */
  function __construct($fromProperty, $toProperty, $targetDir = 'public://') {
    parent::__construct($fromProperty, $toProperty);
    $this->targetDir = $targetDir;
  }

  /**
   * @param EntityMetadataWrapper $fromEntity
   * @param EntityMetadataWrapper $toEntity
   * @return EntityDrupalWrapper|void
   */
  function map(EntityMetadataWrapper $fromEntity, EntityMetadataWrapper $toEntity) {
    $wrapper = $this->getFromValue($fromEntity, TRUE);
    $endpoint = $fromEntity->get("endpoint")->value;
    if (empty($wrapper)) {
      return;
    }
    $value = $wrapper->value();
    if (!empty($value)) {
      $id = $wrapper->get('_id')->value();
      $name = $wrapper->get('name')->value();
      $contents = $endpoint->getFile('download/' . $id);
      if (!empty($contents)) {
        $file = file_save_data($contents, $this->targetDir . '/' . $name);
        $this->setToValue($toEntity, $file);
      }
    }
  }
}
