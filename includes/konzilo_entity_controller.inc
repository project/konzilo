<?php

/**
 * @file
 * Konzilo.
 */

/**
 * Class KonziloEntityController
 */
class KonziloEntityController extends EntityAPIController {

  /**
   * @param array $ids
   * @param array $conditions
   * @return array
   */
  public function load($ids = array(), $conditions = array()) {
    $info = $this->entityInfo;
    // Make sure we have some property info.
    if (!isset($this->propertyInfo)) {
      $this->propertyInfo = entity_get_property_info($this->entityType);
    }

    $entities = array();
    $endpoints = array();
    $numericIds = array();
    // We might come across a mix of new entities, that are not in the mapping
    // table yet, and existing ids, that are numeric. We need to handle both.
    foreach ($ids as $id) {
      if (is_numeric($id)) {
        $numericIds[] = $id;
      }
      else {
        list($endpoint, $entity_id) = explode(':', $id);
        $endpoints[$endpoint][] = $entity_id;
      }
    }
    // Load all mappings from the numeric ids that we have.
    if (!empty($numericIds)) {
      $result = db_select('konzilo_entity_map', 'm')->fields('m')
        ->condition('id', $numericIds)->execute();
      foreach ($result as $map) {
        $endpoints[$map->endpoint][] = $map->resource_id;
      }
    }
    foreach ($endpoints as $endpoint => $ids) {
      // Fetch mappings for this endpoint.
      $mappings = $this->getMappings($ids);
      $endpoint = entity_load_single('konzilo_endpoint', $endpoint);
      // Fetch the entity in one request if we only want one.
      if (count($ids) == 1) {
        $entity = $endpoint->resource($info['resource'])->get($ids[0]);
        if (!empty($entity)) {
          $entity->id = $this->getEntityId($entity, $endpoint->name, $mappings);
          $entity->endpoint = $endpoint->name;
          $this->mapChildren($endpoint->name, $entity, $this->propertyInfo['properties']);
          $entities[$entity->id] = $entity;
        }
      }
      else {
        $query = array(
          '_id' => array('$in' => $ids)
        );
        $response = $endpoint->resource($info['resource'])->query($query);
        foreach ($response as $entity) {
          $entity->id = $this->getEntityId($entity, $endpoint->name, $mappings);
          $entity->endpoint = $endpoint->name;
          $this->mapChildren($endpoint->name, $entity, $this->propertyInfo);
          $entities[$entity->id] = $entity;
        }
      }
    }
    return $entities;
  }

  /**
   * @param $entity
   * @param $endpoint
   * @param $mappings
   * @return mixed
   */
  protected function getEntityId($entity, $endpoint, $mappings) {
    if (is_object($entity)) {
      $entity = $entity->_id;
    }
    if (!empty($mappings[$entity])) {
      return $mappings[$entity];
    }
    else {
      return $this->saveMapping($endpoint, $entity);
    }
  }

  /**
   * @param $endpoint
   * @param $entity
   * @return mixed
   */
  protected function saveMapping($endpoint, $entity) {
    if (is_object($entity)) {
      $entity = $entity->_id;
    }
    $record = array(
      'endpoint' => $endpoint,
      'resource_id' => $entity,
    );
    drupal_write_record('konzilo_entity_map', $record);
    return db_select('konzilo_entity_map', 'm')->fields('m', array('id'))
      ->condition('resource_id', $entity)->execute()->fetchColumn();
  }

  /**
   * A loaded entity from konzilo can contain other entities within it.
   * Those entities needs to be mapped against our mapping table if we are
   * going to use them in the rest of the system.
   */
  protected function mapChildren($endpoint, $entity, $propertyInfo) {
    foreach ($propertyInfo as $name => $info) {
      if (empty($entity->{$name}) || preg_match("/konzilo|struct/", $info['type']) === 0) {
        continue;
      }
      if (strstr($info['type'], 'list') && is_array($entity->{$name})) {
        // Determine the type.
        $matches = array();
        preg_match('/list<(.*)>/', $info['type'], $matches);
        $typeInfo = entity_get_property_info($matches[1]);
        $mappings = $this->getMappings(array_map(function ($entity) {
            return $entity->_id;
          }, $entity->{$name}));
        foreach ($entity->{$name} as $item) {
          $item->id = $this->getEntityId($item, $endpoint, $mappings);
          $item->endpoint = $endpoint;
          $this->mapChildren($endpoint, $item, $typeInfo['properties']);
        }
      }
      elseif (is_object($entity->{$name})) {
        if (isset($entity->{$name}->_id)) {
          $mappings = $this->getMappings($entity->{$name}->_id);
          $entity->{$name}->id = $this->getEntityId($entity->{$name}, $endpoint, $mappings);
          //$item->{$name}->endpoint = $endpoint;
        }
        if ($info['type'] != 'struct') {
          $entity_property_info = entity_get_property_info($info['type']);
          $this->mapChildren($endpoint, $entity->{$name}, $entity_property_info['properties']);
        }
        elseif ($info['property info']) {
          $this->mapChildren($endpoint, $entity->{$name}, $info['property info']);
        }
      }
      elseif (!empty($entity->{$name})) {
        $mappings = $this->getMappings($entity->{$name});
        $this->getEntityId($entity->{$name}, $endpoint, $mappings);
      }
    }
  }

  /**
   * @param $ids
   * @return array
   */
  protected function getMappings($ids) {
    $mappings = array();
    if (!is_array($ids)) {
      $ids = array($ids);
    }
    $result = db_select('konzilo_entity_map', 'm')->fields('m')
      ->condition('resource_id', $ids)->execute();
    foreach ($result as $map) {
      $mappings[$map->resource_id] = $map->id;
    }
    return $mappings;
  }

  /**
   * @param $endpoint
   * @param $entity
   */
  protected function addEndpointInfo($endpoint, $entity) {
    $entity->endpoint = $endpoint->name;
    // All structures should have information about the endpoint as well,
    // it might be needed if we have IDs that are deeply nested.
    foreach ($entity as $name => $property) {
      if (is_object($property)) {
        $this->addEndpointInfo($endpoint, $property);
      }
      if (is_array($property)) {
        foreach ($property as $item) {
          if (!is_object($item)) {
            break;
          }
          $this->addEndpointInfo($endpoint, $item);
        }
      }
    }
  }
}

/**
 * Class KonziloMetadataController
 */
class KonziloMetadataController extends EntityDefaultMetadataController {
  /**
   * @return array
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $info = array();
    $properties = &$info[$this->type]['properties'];
    $properties['_id'] = array(
      'label' => t('Identifier'),
      'description' => t('Unique identifier'),
      'type' => 'string',
    );
    $properties['id'] = array(
      'label' => t('Drupal identifier'),
      'description' => t('The identifier used as unique id by drupal.'),
      'getter callback' => 'konzilo_get_entity_id_property',
      'type' => 'string',
    );
    $properties['endpoint'] = array(
      'label' => t('Endpoint'),
      'description' => t('The endpoint this entity belongs to.'),
      'type' => 'konzilo_endpoint',
    );
    return $info;
  }
}

/**
 * Class KonziloArticlePartMetadataController
 */
class KonziloArticlePartMetadataController extends KonziloMetadataController {
  /**
   * @return array
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];
    $properties['title'] = array(
      'label' => t('Title'),
      'description' => t('Article title'),
      'type' => 'string',
    );
    $properties['teaser'] = array(
      'label' => t('Teaser'),
      'description' => t('Show in listings'),
      'type' => 'boolean',
    );
    $properties['partId'] = array(
      'label' => t('Part ID'),
      'description' => t('The part that this particular revision belongs to'),
      'type' => 'string',
    );
    $properties['article'] = array(
      'label' => t('Article'),
      'description' => t('The article this part belongs to'),
      'getter callback' => 'konzilo_get_entity_property',
      'type' => 'konzilo_article',
    );
    $properties['submitter'] = array(
      'label' => t('User'),
      'description' => t('The creator of this article part'),
      'getter callback' => 'konzilo_get_entity_property',
      'type' => 'konzilo_user',
    );
    $properties['state'] = array(
      'label' => t('State'),
      'description' =>  t('The state of the article part'),
      'type' => 'string',
    );
    $properties['provider'] = array(
      'label' => t('Provider'),
      'description' =>  t('The user tasked with providing this article part.'),
      'getter callback' => 'konzilo_get_entity_property',
      'type' => 'konzilo_user',
    );
    $properties['type'] = array(
      'label' => t('Type'),
      'description' =>  t('The article part type'),
      'type' => 'string',
    );
    $properties['language'] = array(
      'label' => t('Language'),
      'description' =>  t('The article part language'),
      'type' => 'string',
    );
    $properties['terms'] = array(
      'label' => t('Terms'),
      'description' =>  t('The article part language'),
      'type' => 'list<konzilo_file>',
    );
    $properties['comments'] = array(
      'label' => t('Comments'),
      'description' =>  t('Comments for this article part'),
      'type' => 'list<struct>',
    );
    $properties['deadline'] = array(
      'label' => t('Deadline'),
      'description' =>  t('The deadline for this article part'),
      'type' => 'string',
    );

    $properties['byline'] = array(
      'label' => t('Byline'),
      'description' =>  t('The author of this text.'),
      'getter callback' => 'konzilo_get_entity_property',
      'type' => 'konzilo_author',
    );

    $properties['content'] = array(
      'label' => t('Content'),
      'description' =>  t('The content of this article part. Varies depending on type.'),
      'type' => 'struct',
      'property info' => array(
        'images' => array(
          'label' => t('Images'),
          'description' =>  t('A set of images attached to this part.'),
          'type' => 'list<konzilo_file>',
        ),
      ),
    );
    return $info;
  }
}

/**
 * Class KonziloArticleMetadataController
 */
class KonziloArticleMetadataController extends KonziloMetadataController {
  /**
   * @return array
   */
  public function entityPropertyInfo() {
    $info = array();
//    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['title'] = array(
      'label' => t('Title'),
      'description' => t('Article title'),
      'type' => 'string',
    );

    $properties['responsible'] = array(
      'label' => t('Responsible'),
      'type' => 'konzilo_user',
      'description' => t('The user that is reponsible for this article'),
      'getter callback' => 'konzilo_get_entity_property',
    );

    $properties['publishdate'] = array(
      'label' => t('Publish date'),
      'description' => t('publish date'),
      'type' => 'string',
    );

    $properties['unpublishdate'] = array(
      'label' => t('Unpublish date'),
      'description' => t('date when this article should be unpublished'),
      'type' => 'string',
    );

    $properties['keyword'] = array(
      'label' => t('Keyword'),
      'description' => t('The keyword this article belongs to.'),
      'type' => 'string',
    );

    $properties['keywords'] = array(
      'label' => t('Keywords'),
      'type' => 'list<string',
      'description' => t('A list of keywords that this article should match'),
    );

    $properties['comments'] = array(
      'label' => t('Comments'),
      'type' => 'list<struct>',
      'dscription' => t('A list of structs'),
    );

    $properties['description'] = array(
      'label' => t('Description'),
      'type' => 'string',
      'description' => t('A description of what this article is about'),
    );

    $properties['parts'] = array(
      'label' => t('Article parts'),
      'type' => 'list<konzilo_articlepart>',
      'description' => t('A list of article parts attached to this article'),
    );

    $properties['ready'] = array(
      'label' => t('Ready'),
      'type' => 'boolean',
      'description' => t('Is this article ready or not?'),
    );

    $properties['published'] = array(
      'label' => t('Published'),
      'type' => 'boolean',
      'description' => t('Should this article be published?'),
    );

    return $info;
  }
}

/**
 * Class KonziloUserMetadataController
 */
class KonziloUserMetadataController extends KonziloMetadataController {
  /**
   * @return array
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['username'] = array(
      'label' => t('Username'),
      'description' => t('The unique username of this user.'),
      'type' => 'string',
    );

    $properties['email'] = array(
      'label' => t('Email'),
      'description' => t('The user email.'),
      'type' => 'string',
    );

    $properties['active'] = array(
      'label' => t('Active'),
      'description' => t('If the user is active or not.'),
      'type' => 'boolean',
    );

    return $info;
  }
}

/**
 * Class KonziloAuthorMetadataController
 */
class KonziloAuthorMetadataController extends KonziloMetadataController {
  /**
   * @return array
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['name'] = array(
      'label' => t('Na,e'),
      'description' => t('Full name for the author'),
      'type' => 'string',
    );

    $properties['email'] = array(
      'label' => t('Email'),
      'description' => t('The author email.'),
      'type' => 'string',
    );

    $properties['about'] = array(
      'label' => t('About'),
      'description' => t('About the author'),
      'type' => 'string',
    );

    $properties['image'] = array(
      'label' => t('image'),
      'description' => t('The image'),
      'type' => 'konzilo_file',
    );

    return $info;
  }
}

/**
 * Class KonziloFileMetadataController
 */
class KonziloFileMetadataController extends KonziloMetadataController {
  /**
   * @return array
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['uri'] = array(
      'label' => t('URI'),
      'description' => t('The URI'),
      'getter callback' => 'konzilo_get_uri_property',
      'type' => 'uri',
    );

    $properties['name'] = array(
      'label' => t('Name'),
      'description' => t('The file name.'),
      'type' => 'string',
    );

    $properties['username'] = array(
      'label' => t('Username'),
      'description' => t('The username'),
      'type' => 'konzilo_user',
    );

    $properties['type'] = array(
      'label' => t('Type'),
      'description' => t('Type'),
      'type' => 'string',
    );

    $properties['bundle'] = array(
      'label' => t('Bundle'),
      'description' => t('The bundle the file belongs to.'),
      'type' => 'string',
    );

    $properties['created'] = array(
      'label' => t('Created date'),
      'description' => t('The date the file was uploaded.'),
      'type' => 'string',
    );

    return $info;
  }
}

/**
 * @param $data
 * @param array $options
 * @param $name
 * @param $type
 * @param $info
 * @return string
 */
function konzilo_get_uri_property($data, array $options, $name, $type, $info) {
  if (empty($data->{$name}) || empty($data->_id)) {
    return;
  }
  $map = konzilo_get_entity_map($data->_id);
  $endpoint = konzilo_endpoint_load($map->endpoint);
  return url($endpoint->uri . $data->{$name});
}

/**
 * @param $data
 * @param array $options
 * @param $name
 * @param $type
 * @param $info
 * @return mixed
 */
function konzilo_get_entity_property($data, array $options, $name, $type, $info) {
  if (empty($data->{$name})) {
    return;
  }
  if (is_object($data->{$name})) {
    return $data->{$name};
  }
  $map = konzilo_get_entity_map($data->{$name});
  if (!empty($map)) {
    return entity_load_single($info['type'], $map->endpoint . ':' . $data->{$name});
  }
}

/**
 * @param $data
 * @param array $options
 * @param $name
 * @param $type
 * @param $info
 */
function konzilo_get_entity_id_property($data, array $options, $name, $type, $info) {
  if (empty($data->_id) || empty($data->endpoint)) {
    return;
  }
  if (empty($data->{$name})) {
    $data->{$name} = $data->endpoint . ':' . $data->endpoint;
  }
}
