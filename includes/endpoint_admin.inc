<?php
/**
 * @file
 * Admin UI for servers.
 */

/**
 * UI controller.
 */
class KonziloEndpointUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage konzilo endpoints.';
    return $items;
  }

  /**
   * Overrides EntityDefaultUIController::operationForm().
   */
  public function operationForm($form, &$form_state, $entity, $op) {
    if ($op == 'delete') {
      $query = new EntityFieldQuery;
      $query->entityCondition('entity_type', 'oauth2_server_scope');
      $query->propertyCondition('server', $entity->name);
      $query->count();
      $num_scopes = $query->execute();

      $num_clients = 0;
      // If there's at least one scope, we know the delete can't proceed,
      // so no need to count clients.
      if ($num_scopes == 0) {
        $query = new EntityFieldQuery;
        $query->entityCondition('entity_type', 'oauth2_server_client');
        $query->propertyCondition('server', $entity->name);
        $query->count();
        $num_clients = $query->execute();
      }

      if ($num_scopes > 0 || $num_clients > 0) {
        drupal_set_message(t('This server has associated scopes and/or clients, it cannot be deleted.'), 'error');
        return array();
      }
    }

    return parent::operationForm($form, $form_state, $entity, $op);
  }
}

/**
 * Generates the server editing form.
 */
function konzilo_endpoint_form($form, &$form_state, $endpoint, $op = 'edit') {
  if ($op == 'clone') {
    $endpoint->label .= ' (cloned)';
  }
  $form['#tree'] = TRUE;
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => !empty($endpoint->label) ? $endpoint->label : NULL,
    '#description' => t('The human-readable name of this server.'),
    '#required' => TRUE,
  );
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => !empty($endpoint->name) ? $endpoint->name : NULL,
    '#disabled' => (entity_has_status('oauth2_server', $endpoint, ENTITY_IN_CODE) || $op == 'edit'),
    '#machine_name' => array(
      'exists' => 'konzilo_endpoint_load',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this server. It must only contain lowercase letters, numbers, and underscores.'),
  );
  $mappers = entity_mapper_get_mappers();
  $options = array();
  foreach ($mappers as $name => $mapper) {
    $options[$name] = $mapper['label'];
  }
  $form['entity_mapper'] = array(
    '#type' => 'select',
    '#title' => 'Entity mapper',
    '#description' => 'Choose you entity mapper.',
    '#options' => $options,
    '#default_value' => !empty($endpoint->entity_mapper) ? $endpoint->entity_mapper : NULL,
    '#required' => TRUE,
  );
  $form['uri'] = array(
    '#title' => t('URI'),
    '#default_value' => !empty($endpoint->uri) ? $endpoint->uri : NULL,
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('The URI to konzilo'),
  );
  $form['client_id'] = array(
    '#title' => t('Client id'),
    '#default_value' => !empty($endpoint->client_id) ? $endpoint->client_id : NULL,
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('The client key that was generated by konzilo'),
  );
  $form['client_secret'] = array(
    '#title' => t('Client secret'),
    '#type' => 'textfield',
    '#default_value' => !empty($endpoint->client_secret) ? $endpoint->client_secret : NULL,
    '#required' => TRUE,
    '#description' => t('The client secret that was generated by konzilo'),
  );
  if ($op == 'edit') {
    $form['authorize'] = array(
      '#type' => 'submit',
      '#value' => t('Authorize'),
      '#submit' => array('konzilo_endpoint_form_authorize'),
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save endpoint'),
    '#weight' => 40,
  );

  if (!entity_has_status('konzilo_endpoint', $endpoint, ENTITY_IN_CODE)
      && !in_array($op, array('add', 'clone'))) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete endpoint'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('oauth2_server_form_submit_delete')
    );
  }
  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function konzilo_endpoint_form_authorize($form, $form_state) {
  $query = array(
    'response_type' => 'code',
    'client_id' => $form_state['values']['client_id'],
    'state' => drupal_get_token($form_state['values']['client_id']),
    // The "authorized" url doesn't actually exist, but we don't need it.
    'redirect_uri' => url('/konzilo/authorized/' . $form_state['values']['name'], array('absolute' => TRUE)),
  );
  $uri = $form_state['values']['uri'] . '/oauth2/authorize';
  drupal_goto(url($uri, array('query' => $query, 'absolute' => TRUE)));
}

/**
 * Validation callback.
 */
function konzilo_endpoint_form_validate($form, &$form_state) {
  entity_form_field_validate('konzilo_endpoint', $form, $form_state);
}

/**
 * Form API submit callback for the type form.
 */
function konzilo_endpoint_form_submit(&$form, &$form_state) {
  $endpoint = entity_ui_form_submit_build_entity($form, $form_state);
  $endpoint->save();
  $form_state['redirect'] = 'admin/structure/konzilo-endpoints';
}

/**
 * Form API submit callback for the delete button.
 */
function konzilo_endpoint_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/konzilo_endpoint/manage/' . $form_state['konzilo_endpoint']->name . '/delete';
}
