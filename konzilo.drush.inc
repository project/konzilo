<?php

/**
 * @file
 * Konzilo.
 */

/**
 * Implements hook_drush_command().
 */
function konzilo_drush_command() {
  $items = array();

  $items['konzilo-create-client'] = array(
    'description' => "Create a client for konzilo.",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL,
    'aliases' => array('kcc'),
  );
  $items['konzilo-create-endpoint'] = array(
    'description' => "Create a endpoint for konzilo.",
    'arguments' => array(
      'client_id' => 'The client id',
      'client_secret' => 'The client secret',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL,
    'aliases' => array('kce'),
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function konzilo_drush_help($section) {
  switch ($section) {
    case 'drush:konzilo-create-client':
      return dt("Creates a client for Konzilo in Drupal.");
    case 'drush:konzilo-create-endpoint':
      return dt("Creates a endpoint for Konzilo in Drupal.");
  }
}


/**
 * Create a konzilo client in Drupal.
 */
function drush_konzilo_create_client() {
  drush_include_engine('drupal', 'environment');
  $client = new stdClass();
  $client->server = 'konzilo';
  $client->label = 'konzilo_kickstart';
  $client->client_key = konzilo_randomString();
  $client->client_secret = konzilo_randomString();
  $client->redirect_uri = 'http://konzilo:3000/endpoint/konzilo_kickstart/authorized';
  $client->automatic_authorization = 1;

  $result = db_select('oauth2_server_client', 'c')
    ->fields('c', array('redirect_uri', 'label', 'server', 'client_id'))
    ->condition('redirect_uri', $client->redirect_uri)
    ->condition('label', $client->label)
    ->condition('server', $client->server)
    ->execute()
    ->fetch();

  if ($result) {
    $client->client_id = $result->client_id;
    drupal_write_record('oauth2_server_client', $client, array('client_id'));
  }
  else {
    drupal_write_record('oauth2_server_client', $client);
  }
  // Output the result to be taken care of on the konzilo side.
  echo drupal_json_output($client);
}

/**
 * Create a konzilo client in Drupal.
 */
function drush_konzilo_create_endpoint($client_id = NULL, $client_secret = NULL) {
  drush_include_engine('drupal', 'environment');
  $endpoint = new stdClass();

  $endpoint->label = 'Konzilo Kickstart';
  $endpoint->name = 'konzilo_kickstart';
  $endpoint->uri = 'http://konzilo:3000';
  $endpoint->entity_mapper = 'konzilo_article';
  $endpoint->client_id = $client_id;
  $endpoint->client_secret = $client_secret;
  $endpoint->status = 1;
  $endpoint->module = NULL;
  $endpoint->access_token = NULL;
  $endpoint->refresh_token = NULL;


  $result = db_select('konzilo_endpoint', 'k')
    ->fields('k', array('name', 'eid'))
    ->condition('name', $endpoint->name)
    ->execute()
    ->fetch();

  if ($result) {
    $endpoint->eid = $result->eid;
//    $endpoint->access_token = $result->access_token;
//    $endpoint->refresh_token = $result->refresh_token;
    drupal_write_record('konzilo_endpoint', $endpoint, array('eid'));
  }
  else {
    drupal_write_record('konzilo_endpoint', $endpoint);
  }
  // Output the result to be taken care of on the konzilo side.
  echo drupal_json_output($endpoint);
}

/**
 * Helper function to generate a random string.
 * @param int $length
 * @return string
 */
function konzilo_randomString($length = 16) {
  $values = array_merge(range(65, 90), range(97, 122), range(48, 57));
  $max = count($values) - 1;
  $str = chr(mt_rand(97, 122));
  for ($i = 1; $i < $length; $i++) {
    $str .= chr($values[mt_rand(0, $max)]);
  }
  return $str;
}